import os

seaPath = './mer/'
otherPath = './autre/'


# Imprime l'ensemble des noms de fichiers contenus dans le tableau files
def print_files(files):
    for file in files:
        print(file)


# Renomme les images
def rename_images(path, prefix):
    i = 0
    for file in os.listdir(path):
        print(file)
        complete = file.split('.')
        ext = complete[1]
        print(ext)
        os.rename(path + file, path + prefix + str(i) + '.' + ext)
        i += 1


rename_images(seaPath, 'mer')
rename_images(otherPath, 'autre')
