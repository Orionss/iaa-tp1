import glob
import os
import sys

import cv2 as cv
import numpy as np
from joblib import load


def getBlueHist(image):
    return cv.calcHist([image], [2], None, [256], [0, 256])


def getHistogramsMatrix(images):
    histograms = []
    for img in images:
        histograms.append(getBlueHist(img))  # ,getRedHist(img), getGreenHist(img),
    return np.asarray(histograms)


def flat_image(image):
    return image.flatten()


def flat_images(images):
    flatten_dataset = []
    for img in images:
        flatten_dataset.append(flat_image(img))
    return np.asarray(flatten_dataset)


def flatHistograms(histsList):
    return histsList.flatten()  # , histsList[1].flatten(), histsList[2].flatten()


def flatHistoList(hists):
    flatten = []
    for line in hists:
        flatten.append(flatHistograms(line))
    return np.asarray(flatten)


def get_image(name, size):
    img = cv.resize(cv.imread(name), size)
    return cv.cvtColor(img, cv.COLOR_BGR2RGB)  # plt.imread(name)[:,:,:3]


def get_images(datas_names, size):
    images = []
    targets = []
    for name in datas_names:
        img = get_image(name, size)
        target = 1 if "mer" in name else 0
        images.append(img)
        targets.append(target)
    return np.asarray(images), np.asarray(targets)


def preprocessing(images):
    hists = getHistogramsMatrix(images)
    images = flat_images(images)
    flattenhists = flatHistoList(hists)
    inputs = np.concatenate((images, flattenhists), axis=1)
    return np.asarray(inputs)


def convert_classes(predictions):
    classes = []
    for prediction in predictions:
        if prediction == 0:
            classes.append('autre')
        else:
            classes.append('mer')
    return classes


def predict_main(path):
    size = (100, 100)
    images_names = os.listdir(path)
    datas = glob.glob(path + '/*')
    images, y = get_images(datas, size)
    X = preprocessing(images)

    clf = load('loic.joblib')
    predicted_label = clf.predict(X)
    predicted_label_lisible = convert_classes(predicted_label)

    return [images_names, predicted_label_lisible]


if __name__ == "__main__":
    print(predict_main(sys.argv[1]))
