import os
import random

from sklearn import svm
from sklearn.decomposition import PCA
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.preprocessing import StandardScaler
from joblib import dump

from ImageDataset import ImageDataset, ImageDatasetClass

URL_SEA = '.\\Data\\Mer\\'
URL_ELSEWHERE = '.\\Data\\Ailleurs\\'

# URL_SEA = './mer/'
# URL_ELSEWHERE = './autre/'

dataset = []

for sea_pic_url in os.listdir(URL_SEA):
    data = ImageDataset.fromFileName((URL_SEA + sea_pic_url), ImageDatasetClass.SEA)
    dataset.append(data.create_variation())
    dataset.append(data)
for elsewhere_pic_url in os.listdir(URL_ELSEWHERE):
    data = ImageDataset.fromFileName((URL_ELSEWHERE + elsewhere_pic_url), ImageDatasetClass.ELSEWHERE)
    dataset.append(data.create_variation())
    dataset.append(data)

random.shuffle(dataset)
X = []
y = []
for data in dataset:
    X.append(data.normalize_X())
    y.append(data.normalize_y())

gnb = GaussianNB()
clf = svm.SVC()

params = {
    "kernel": ["rbf"],
    "C": [x for x in range(1, 20)]
}

pca = PCA(n_components=15)
pca.fit(X)
X_pca = pca.transform(X)

scaler = StandardScaler()
scaler.fit(X_pca)
scaled_X = scaler.transform(X_pca)

X_train, X_test, y_train, y_test = train_test_split(scaled_X, y, test_size=0.2, random_state=0)

model = GridSearchCV(clf, params, cv=5, verbose=3)
model.fit(X_train, y_train)
print(model.score(X_test, y_test))
test_scores = model.cv_results_["mean_test_score"]
print(model.best_estimator_)

# scores = cross_val_score(clf, X_test, y_test, cv=5)
print(test_scores)
# print("Max : " + str(max(scores)))
# print("Min : " + str(min(scores)))
# print(scores.mean())

dump(model.best_estimator_, 'monclf.joblib')
