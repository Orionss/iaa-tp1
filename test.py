import matplotlib.pyplot as plt

from drone import predict_main

path = 'images_to_predict'
predictions = predict_main(path)

total = len(predictions[0])
good = 0


def print_image(image, label):
    plt.title(label)
    plt.imshow(plt.imread(image))
    plt.axis('off')
    plt.show()


for i in range(total):
    print(predictions[0][i] + ' is ' + predictions[1][i])
    if predictions[1][i] in (predictions[0][i]):
        good += 1

    print_image(path + '/' + predictions[0][i], predictions[1][i])

print(str(good) + ' images bonnes sur ' + str(total) + ' images')
print(good/total)
