from enum import Enum
import cv2 as cv
import numpy as np
import copy
import random

CANNY_PARAMS = (180, 200)
SRC_IMG_DIMENSIONS = (35, 35)
IMG_VARIATION = 5


class ImageDataset:
    def __init__(self, pixel_array, gray_scale, category):
        self.pixel_array = pixel_array
        self.gray_scale = gray_scale
        self.category = category
        self.get_hist()
        # self.generate_canny_edge_detection()
        # self.get_sift()

    @classmethod
    def fromFileName(cls, url, category):
        pixel_array, gray_scale = ImageDataset.get_image(url)
        return ImageDataset(pixel_array, gray_scale, category)

    def generate_canny_edge_detection(self):
        self.canny = cv.Canny(self.gray_scale, CANNY_PARAMS[0], CANNY_PARAMS[1])

    @classmethod
    def get_image(cls, url):
        pixel_array = cv.imread(url, cv.IMREAD_COLOR)
        pixel_array = cv.resize(pixel_array, SRC_IMG_DIMENSIONS)
        gray_scale = cv.imread(url, cv.IMREAD_GRAYSCALE)
        gray_scale = cv.resize(gray_scale, SRC_IMG_DIMENSIONS)
        return pixel_array, gray_scale

    def create_variation(self):
        img = copy.deepcopy(self.pixel_array)
        nb_occ = SRC_IMG_DIMENSIONS[0] * SRC_IMG_DIMENSIONS[1] * IMG_VARIATION / 100
        nb_occ = round(nb_occ)
        for i in range(0, nb_occ):
            x = random.randint(0, SRC_IMG_DIMENSIONS[0] - 1)
            y = random.randint(0, SRC_IMG_DIMENSIONS[1] - 1)
            img[x][y][0] = 0
            img[x][y][1] = 0
            img[x][y][2] = 0
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        return ImageDataset(img, gray, self.category)

    def get_hist(self):
        bgr_planes = cv.split(self.pixel_array)
        histSize = 256
        histRange = (0, 256)
        accumulate = False
        self.b_hist = cv.calcHist(bgr_planes, [0], None, [histSize], histRange, accumulate=accumulate)

    def get_sift(self):
        sift = cv.SIFT_create()
        img = np.zeros((SRC_IMG_DIMENSIONS[0], SRC_IMG_DIMENSIONS[1], 3), dtype="uint8")
        kp = sift.detect(self.gray_scale, None)
        self.sift = cv.drawKeypoints(img, kp, self.pixel_array)

    def normalize_X(self):
        result = []
        self.normalize_include_colored_picture(result)
        # self.normalize_include_canny(result)
        self.normalize_include_blue_histogram(result)
        # self.normalize_include_sift(result)
        return result

    def normalize_include_colored_picture(self, result):
        for x in self.pixel_array:
            for y in x:
                for color in y:
                    result.append(color)

    def normalize_include_sift(self, result):
        for x in self.sift:
            for y in x:
                for color in y:
                    result.append(color)

    def normalize_include_canny(self, result):
        for x in self.canny:
            for y in x:
                result.append(y)

    def normalize_include_blue_histogram(self, result):
        for value in self.b_hist:
            result.append(value[0])

    def normalize_y(self):
        return self.category.value


class ImageDatasetClass(Enum):
    ELSEWHERE = 0
    SEA = 1
    UNDEFINED = 2
