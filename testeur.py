import os
import random

from joblib import load
from sklearn.decomposition import PCA
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

from ImageDataset import ImageDataset, ImageDatasetClass

URL_SEA = './mer/'
URL_ELSEWHERE = './autre/'

dataset = []

for sea_pic_url in os.listdir(URL_SEA):
    data = ImageDataset.fromFileName((URL_SEA + sea_pic_url), ImageDatasetClass.SEA)
    dataset.append(data)
for elsewhere_pic_url in os.listdir(URL_ELSEWHERE):
    data = ImageDataset.fromFileName((URL_ELSEWHERE + elsewhere_pic_url), ImageDatasetClass.ELSEWHERE)
    dataset.append(data)

X = []
y = []
for data in dataset:
    X.append(data.normalize_X())
    y.append(data.normalize_y())

pca = PCA(n_components=15)
pca.fit(X)
X_pca = pca.transform(X)

scaler = StandardScaler()
scaler.fit(X_pca)
scaled_X = scaler.transform(X_pca)

clf = load('monclf.joblib')

targets = clf.predict(scaled_X)

tn, fp, fn, tp = confusion_matrix(y, targets).ravel()

print('vrai négatif ' + str(tn))
print('faux posiif ' + str(fp))
print('faux négatif ' + str(fn))
print('vrai positif ' + str(tp))
print('total ' + str(tn + fp + fn + tp))


def compute_metrics(tp, tn, fp, fn):
    precision = tp / (tp + fp)
    rappel = tp / (tp + fn)
    f_score = 2 * precision * rappel / (precision + rappel)
    accuracy = (tp + tn) / (tp + tn + fp + fn)
    return (precision, rappel, f_score, accuracy)


precision, recall, f_score, accuracy = compute_metrics(tp, tn, fp, fn)

print('precision ' + str(precision))
print('recall ' + str(recall))
print('f_score ' + str(f_score))
print('accuracy ' + str(accuracy))
