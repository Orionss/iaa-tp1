import os
import sys

from joblib import load
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

from ImageDataset import ImageDataset, ImageDatasetClass


def convert_classes(predictions):
    classes = []
    for prediction in predictions:
        if prediction == 0:
            classes.append('autre')
        else:
            classes.append('mer')
    return classes


def predict_main(path):
    dataset = []
    images = []
    for file in os.listdir(path):
        images.append(file)
        data = ImageDataset.fromFileName((path + '/' + file), ImageDatasetClass.UNDEFINED)
        dataset.append(data)

    pictures = []
    for data in dataset:
        pictures.append(data.normalize_X())

    pca = PCA(n_components=15)
    pca.fit(pictures)
    picture_pca = pca.transform(pictures)

    scaler = StandardScaler()
    scaler.fit(picture_pca)
    scaled_pictures = scaler.transform(picture_pca)

    clf = load('monclf.joblib')
    predicted_label = clf.predict(scaled_pictures)
    predicted_label_lisible = convert_classes(predicted_label)

    return [images, predicted_label_lisible]


if __name__ == "__main__":
    # execute only if run as a script
    print(predict_main(sys.argv[1]))
